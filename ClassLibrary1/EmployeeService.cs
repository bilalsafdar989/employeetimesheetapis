﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using timesheet.business.ViewModel;
using timesheet.data;
using timesheet.model;
 

namespace timesheet.business
{
    public class EmployeeService
    {
        public TimesheetDb db { get; }
        public EmployeeService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }

        public IQueryable<Employee> GetEmployees()
        {
            return this.db.Employees;
        }

        public List<EmployeeDetailsRecordsModel> GetEmployeesWithDetails()
        {
            var temp = db.Employees;

            int days = DateTime.Now.DayOfWeek - DayOfWeek.Sunday;
            DateTime weekStart = DateTime.Now.AddDays(-days);
            DateTime weekEnd = weekStart.AddDays(6);
            //GET CURRENT WEEK EFFORT LOG FOR EMPLOYEES
            var EmployeeLogs = (from logs in db.EmployeesEffortLog
                                where logs.CreatedDate.Date >= weekStart.Date && logs.CreatedDate.Date <= weekEnd.Date
                                group logs by logs.EmployeeId into grp
                                select new { EmployeeId = grp.Key, Hours = grp.Sum(x=>x.Hours) }).ToList();

            //GER WEEKLY PROJECTION OF EMPLOYEES LOG
            Func<DateTime, int> weekProjector = d => CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(d, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Sunday);
            

            List<EmployeeDetailsRecordsModel> returnlist = new List<EmployeeDetailsRecordsModel>();
            decimal AverageWeeklyEffort = 0;
            foreach (var item in temp)
            {
                //SELECT THE CURRENT EMPLOYEE FROM WEEKLY EFFORT PROJECTION
                var consignmentsByWeek = from con in db.EmployeesEffortLog
                                         where con.EmployeeId == item.Id
                                         group con by weekProjector(con.CreatedDate);
                //WEEKLY EFFORT LOG EXIST IN DATABASE
                var list = consignmentsByWeek.ToList();
                //IF LOG EXIST FOR THE EMPLOYEE
                if (list.Count() > 0)
                {
                    //GET THE AVERGARE OF WEEKLY EFFORT LOG
                    int TotalEffortLog = list.SelectMany(group => group).Sum(p => p.Hours);
                    AverageWeeklyEffort = TotalEffortLog / list.Count;
                }
                EmployeeDetailsRecordsModel record = new EmployeeDetailsRecordsModel()
                {
                    Id = item.Id,
                    Name = item.Name,
                    Code = item.Code,
                    AverageWeeklyEffort = AverageWeeklyEffort
                };
                //ASSIGN THE CURRENT WEEKLY EFFORT
                var extra = EmployeeLogs.Where(x => x.EmployeeId == item.Id).FirstOrDefault();
                if (extra != null)
                { 
                    record.CurrentWeeklyEffort = extra.Hours;
                }
                //ADD THE EMPLOYEE TO THE LIST
                returnlist.Add(record);
                //RESET THE AVERAGE
                AverageWeeklyEffort = 0;
            }
            
            return returnlist;
        }
    }
}
