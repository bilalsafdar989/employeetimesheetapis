﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace timesheet.business.ViewModel
{
    public class EmployeeEffortLogModel
    {
        public int EmployeeId { get; set; }
        public int TaskId { get; set; }
        public int Hours { get; set; }
        public DateTime CreatedDate { get; set; }
        public string DayName { get; set; }
    }
}
