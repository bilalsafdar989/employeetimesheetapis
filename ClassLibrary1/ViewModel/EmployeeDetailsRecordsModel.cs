﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace timesheet.business.ViewModel
{
    public class EmployeeDetailsRecordsModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int CurrentWeeklyEffort { get; set; }
        public decimal AverageWeeklyEffort { get; set; }
    }
}
