﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace timesheet.business.ViewModel
{
    public class EmployeeTimeSheetModel
    {
        public string TaskName { get; set; }
        public List<EmployeeEffortLogModel> EmployeeEffortLogs { get; set; }   
    }
}
