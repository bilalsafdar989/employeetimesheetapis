﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using timesheet.data;
using timesheet.model;
using timesheet.business.ViewModel;
using System.Globalization;
using System.Collections.Generic;

namespace timesheet.business
{
    public class EmployeeEffortLogService
    {
        public TimesheetDb db { get; }
        public EmployeeEffortLogService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }

        public IEnumerable<EmployeeTimeSheetModel> EmployeeTimeSheet(int EmployeeId, int Index)
        {
            //GET THE CURRENT WEEK START DATE
            DateTime start = GetStartDateofWeek(Index),
            end = start.AddDays(7); // next sunday 00:00
                                    //GET WEEKLY EFFORT LOG
            var WeeklyEffortLog = this.db.EmployeesEffortLog.
               Include(_ => _.Employee).
               Include(_ => _.Task).
               Where(x => x.EmployeeId == EmployeeId && (x.CreatedDate >= start && x.CreatedDate < end));

            //GROUPING THE EFORT LOG BY TASK
            var TasksGrp = WeeklyEffortLog.GroupBy(p => p.Task.Name).ToList();
            List<EmployeeTimeSheetModel> returnlist = new List<EmployeeTimeSheetModel>();
            foreach (var item in TasksGrp)
            {
                EmployeeTimeSheetModel record = new EmployeeTimeSheetModel()
                {
                     TaskName= item.Key
                };
                var EffortLogs = item.ToList();
                List<EmployeeEffortLogModel> Effortlist = new List<EmployeeEffortLogModel>();
                foreach(EmployeeEffortLog employeeEffortLog in EffortLogs)
                {
                    EmployeeEffortLogModel effort = new EmployeeEffortLogModel()
                    {
                        TaskId = employeeEffortLog.TaskId,
                        CreatedDate = employeeEffortLog.CreatedDate,
                        EmployeeId = employeeEffortLog.EmployeeId,
                        Hours= employeeEffortLog.Hours,
                        DayName= employeeEffortLog.CreatedDate.DayOfWeek.ToString()
                    };
                    Effortlist.Add(effort);
                }
                Effortlist = Effortlist.OrderBy(p => p.CreatedDate.Day).ToList();
                record.EmployeeEffortLogs = Effortlist;
                returnlist.Add(record);
            }
            return returnlist;
        }
        public DateTime GetStartDateofWeek (int Index)
        {
            int WeekNumber = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            //ADD THE INDEX
            WeekNumber = WeekNumber + Index;
            // set date to the first day of the year
            int year = DateTime.Now.Year;
            DateTime dt = new DateTime(year, 1, 1);
            // mult by 7 to get the day number of the year
            int days = (WeekNumber - 1) * 7;
            // get the date of that day
            DateTime dt1 = dt.AddDays(days);
            // check what day of week it is
            DayOfWeek dow = dt1.DayOfWeek;
            // to get the first day of that week - subtract the value of the DayOfWeek enum from the date
            DateTime startDateOfWeek = dt1.AddDays(-(int)dow);
            //RETURN START DATE OF THE WEEK
            return startDateOfWeek;
        }
        public EmployeeEffortLog AddEmployeeTimeSheet(EmployeeEffortLogModel NewTimeSheetModel)
        {
            //MAKE SURE THAT WE DO NOT DUPLICATE THE SAME TASK ON SAME DATE FOR SELECTED EMPLOYEE
            if(this.db.EmployeesEffortLog.Where(p => p.EmployeeId== NewTimeSheetModel.EmployeeId && p.TaskId== NewTimeSheetModel.TaskId && p.CreatedDate.Date == NewTimeSheetModel.CreatedDate.Date).Any())
            {
                return null;
            }
            EmployeeEffortLog EmployeeEffortLog = new EmployeeEffortLog();
            //CREATE A NEW EFFORT LOG FOR EMP
            EmployeeEffortLog.EmployeeId = NewTimeSheetModel.EmployeeId;
            EmployeeEffortLog.TaskId = NewTimeSheetModel.TaskId;
            EmployeeEffortLog.Hours = NewTimeSheetModel.Hours;
            EmployeeEffortLog.CreatedDate = NewTimeSheetModel.CreatedDate;
            this.db.EmployeesEffortLog.Add(EmployeeEffortLog);
            this.db.SaveChanges();
            return EmployeeEffortLog;
        }
        public EmployeeEffortLog UpdateEmployeeTimeSheet(EmployeeEffortLogModel NewTimeSheetModel)
        {
            EmployeeEffortLog EmployeeEffortLog = this.db.EmployeesEffortLog.Where(p => p.EmployeeId == NewTimeSheetModel.EmployeeId && p.TaskId == NewTimeSheetModel.TaskId && p.CreatedDate.Date == NewTimeSheetModel.CreatedDate.Date).FirstOrDefault();
            //UPDATE EFFORT LOG FOR EMP
            EmployeeEffortLog.Hours = NewTimeSheetModel.Hours;
            EmployeeEffortLog.CreatedDate = NewTimeSheetModel.CreatedDate;
            this.db.EmployeesEffortLog.Update(EmployeeEffortLog);
            this.db.SaveChanges();
            return EmployeeEffortLog;
        }
    }
}
