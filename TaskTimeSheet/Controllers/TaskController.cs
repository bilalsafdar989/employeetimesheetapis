﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;

namespace TaskTimeSheet.Controllers
{
    [Route("api/task")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly TaskService taskService;
        public TaskController(TaskService TaskService)
        {
            this.taskService = TaskService;
        }
        [HttpGet("getall")]
        public IActionResult GetAll(string text)
        {
            var items = this.taskService.GetTasks();
            return new ObjectResult(items);
        }
    }
}
