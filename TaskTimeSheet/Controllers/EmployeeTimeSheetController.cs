﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;
using timesheet.business.ViewModel;

namespace TaskTimeSheet.Controllers
{
    [Route("api/timesheet")]
    [ApiController]
    public class EmployeeTimeSheetController : ControllerBase
    {
        private readonly EmployeeEffortLogService timesheetService;
        public EmployeeTimeSheetController(EmployeeEffortLogService TimeSheetService)
        {
            this.timesheetService = TimeSheetService;
        }
        [HttpGet("getemployeetimesheetbyid")]
        public IActionResult GetEmployeeTimeSheet(int EmployeeId,int Index)
        {
            var items = this.timesheetService.EmployeeTimeSheet(EmployeeId, Index).ToList();
            return new ObjectResult(items);
        }
        [HttpPost("addemployeetimesheet")]
        public IActionResult AddEmployeeTimeSheet(EmployeeEffortLogModel NewTimeSheetModel)
        {
            var item = this.timesheetService.AddEmployeeTimeSheet(NewTimeSheetModel);
            if (item == null)
            {
                const string msg = "Effort Log Alraedy Exist For Employee For The Same Day and Task Selected!";
                return StatusCode((int)HttpStatusCode.Conflict, msg);
            }
            return new ObjectResult(item);
        }
        [HttpPost("updateemployeetimesheet")]
        public IActionResult UpdateEmployeeTimeSheet(EmployeeEffortLogModel NewTimeSheetModel)
        {
            var item = this.timesheetService.UpdateEmployeeTimeSheet(NewTimeSheetModel);
            return new ObjectResult(item);
        }
    }
}
