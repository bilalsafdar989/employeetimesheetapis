﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;

namespace timesheet.api.controllers
{
    [Route("api/employee")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly EmployeeService employeeService;
        public EmployeeController(EmployeeService employeeService)
        {
            this.employeeService = employeeService;
        }

        [HttpGet("getall")]
        public IActionResult GetAll()
        {
            var items = this.employeeService.GetEmployees();
            return new ObjectResult(items);
        }
        [HttpGet("getallempdetails")]
        public IActionResult getallempdetails()
        {
            var items = this.employeeService.GetEmployeesWithDetails();
            return new ObjectResult(items);
        }
    }
}